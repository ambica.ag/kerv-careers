
### Installation (pre-requisites)

1. JDK 1.8+ (make sure Java class path is set)
2. Maven (make sure .m2 class path is set)
3. Eclipse
4. Eclipse Plugins for
    - Maven
    - Cucumber
    - testNG

### Framework set up

Clone repository from (https://gitlab.com/ambica.ag/kerv-careers.git) and set it up in your local workspace.


### Run test

Open command prompt and navigate to the project directory
type `mvn clean test -Dtestng.dtd.http=true` command to run feature. With this command it will invoke the default chrome browser and will
execute the tests.

- To run features on specific browser use, change browser property in data.properties file
  browser_name can be one of following
    - chrome
    - edge
    - firefox

### Reports

Once tests are run,reports can be viewed in ExtentReports folder. 
