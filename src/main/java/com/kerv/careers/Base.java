package com.kerv.careers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Base {

	private static final Logger LOGGER = LoggerFactory.getLogger(Base.class);
	
	protected WebDriver driver;
	protected Properties prop;
	protected WebDriverWait wait;

	public void initializeDriver() throws IOException {

		loadProperties();

		String browserName = prop.getProperty("browser");

		if ("chrome".equals(browserName)) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		} else if ("firefox".equals(browserName)) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		} else if ("edge".equals(browserName)) {
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
		} else {
			LOGGER.error("Browser "+browserName+" is not supported");
			System.exit(0);
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		LOGGER.info("Driver Initialized");
	}

	private void loadProperties() throws FileNotFoundException, IOException {
		prop = new Properties();
		File file = new File(getClass().getClassLoader().getResource("data.properties").getFile());
		FileInputStream fis = new FileInputStream(file);
		prop.load(fis);
	}

	public void visitHomePage() {
		String url = prop.getProperty("url");
		if(url !=null) {
			driver.get(url);	
		} else {
			LOGGER.error("Homepage url not defined");
		}
		
	}
	
	public void cleanUp(){
		LOGGER.info("Closing the browser windows");
		if (driver != null) {
            driver.quit();
        }
	}

}
