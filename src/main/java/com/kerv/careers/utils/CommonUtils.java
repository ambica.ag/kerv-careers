package com.kerv.careers.utils;

import java.time.Duration;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonUtils {

	public static void acceptCookies(WebDriver driver, String value) {

		By cookies_accept = By.xpath("//a[@role='button' and normalize-space(text())='" + value + "']");
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(cookies_accept)).click();

	}

	public static void switchWindow(WebDriver driver) {
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allChildWindowHandles = driver.getWindowHandles();
		for (String ChildWindow : allChildWindowHandles) {
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);

			}

		}
	}

}
