package com.kerv.careers.page;

import java.io.File;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class JobApplyPage {

	private WebDriver driver;
	private JavascriptExecutor jse;
	private WebDriverWait wait;

	private By firstNameTextBox = By.xpath("//label[text()='First name']//following-sibling::div//input");
	private By lastNameTextBox = By.xpath("//label[text()='Last name']//following-sibling::div//input");
	private By emailTextBox = By.xpath("//label[text()='Email']//following-sibling::div//input");
	private By phoneTextBox = By.xpath("//label[text()='Phone']//following-sibling::div//input");
	private By coverLetterUploadBtn = By.xpath("//label[text()='Cover letter']//following-sibling::div//input[@type='file']");
	private By CVUploadBtn = By.xpath("//label[text()='CV']//following-sibling::div//input[@type='file']");
	private By iFrame = By.xpath("//iframe[@title='reCAPTCHA']");
	private By reCaptchaCheckBox = By.xpath("//div[@class='recaptcha-checkbox-border']");
	private By submitBtn = By.xpath("//span[text()='Submit']//ancestor::button");
	private By successMsg = By.cssSelector("p.header-simple__description");

	public JobApplyPage(WebDriver driver) {
		this.driver = driver;
		this.jse = (JavascriptExecutor) driver;
		this.wait = new WebDriverWait(driver, Duration.ofSeconds(5));
	}

	public void enterFirstName(String firstName) {
		driver.findElement(firstNameTextBox).sendKeys(firstName);
	}

	public void enterLastName(String lastName) {
		driver.findElement(lastNameTextBox).sendKeys(lastName);
	}

	public void enterEmail(String email) {
		driver.findElement(emailTextBox).sendKeys(email);
	}

	public void enterPhone(String phone) {
		driver.findElement(phoneTextBox).sendKeys(phone);
	}

	public void uploadCoverLetter(String coverLetterPath) {
		File file = new File(coverLetterPath);
		driver.findElement(coverLetterUploadBtn).sendKeys(file.getAbsolutePath());
	}

	public void uploadCV(String cvPath) {
		File file = new File(cvPath);
		driver.findElement(CVUploadBtn).sendKeys(file.getAbsolutePath());
	}

	public void selectReCaptchaBox() {
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iFrame));
		wait.until(ExpectedConditions.elementToBeClickable(reCaptchaCheckBox));
		WebElement recaptchaBox = driver.findElement(reCaptchaCheckBox);
		jse.executeScript("arguments[0].click();", recaptchaBox);
		driver.switchTo().parentFrame();
	}

	public void submit() {
		WebElement submitButton =driver.findElement(submitBtn);
		jse.executeScript("arguments[0].click();", submitButton);
	}
	
	public void verifySuccessfulSubmission(String expectedMessage){
		String actualSuccessMessage = driver.findElement(successMsg).getText().trim();
		Assert.assertEquals(actualSuccessMessage,expectedMessage);
	}
}
