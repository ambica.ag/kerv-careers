package com.kerv.careers.page;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;

public class HomePage {

	private WebDriver driver;
	private WebDriverWait wait;
	private Actions actions;
	private String menuItemXpath = "//div[contains(@class,'navbar__root')]//descendant::a[@href='#' and normalize-space(text())='%s']";
	private String careersDropdownOptionsXpath = "//div[@class='navbar__sub-big-list']/a[text()='%s']";

	public HomePage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		this.actions = new Actions(driver);

	}

	public void hoverOnMenuItem(String menuItemValue) {
		By menuItem = By.xpath(String.format(menuItemXpath, menuItemValue));
		WebElement careerLink = driver.findElement(menuItem);
		actions.moveToElement(careerLink).build().perform();
	}

	public void clickCareerOption(String careersDropDownValue) {
		By careersDropdown = By.xpath(String.format(careersDropdownOptionsXpath, careersDropDownValue));
		wait.until(ExpectedConditions.elementToBeClickable(careersDropdown)).click();

	}

}