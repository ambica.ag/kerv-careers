package com.kerv.careers.page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JobOpportunitiesPage {

	private WebDriver driver;
	private JavascriptExecutor jse;

	private By searchVacanyTextBox = By.xpath("//input[@placeholder='Search vacancies here']");
	private By searchButton = By.xpath("//button[@title='Search vacancies' and normalize-space(text())='Search']");
	private String searchResult = "//div[@class='search-results__list']//descendant::div[normalize-space(text())='%s']//preceding-sibling::a";

	public JobOpportunitiesPage(WebDriver driver) {
		this.driver = driver;
		this.jse = (JavascriptExecutor) driver;
	}

	public void searchVacancies(String role) {

		WebElement searchVacancy = driver.findElement(searchVacanyTextBox);
		searchVacancy.sendKeys(role);
		WebElement searchButton1 = driver.findElement(searchButton);
		jse.executeScript("arguments[0].click();", searchButton1);
	}

	public void clickJobLink(String role) {
		By jobLink = By.xpath(String.format(searchResult, role));
		WebElement vacancyLinkBox = driver.findElement(jobLink);
		jse.executeScript("arguments[0].scrollIntoView();", vacancyLinkBox);
		jse.executeScript("arguments[0].click();", vacancyLinkBox);
	}

}
