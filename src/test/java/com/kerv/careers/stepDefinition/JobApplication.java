package com.kerv.careers.stepDefinition;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.kerv.careers.Base;
import com.kerv.careers.page.HomePage;
import com.kerv.careers.page.JobApplyPage;
import com.kerv.careers.page.JobOpportunitiesPage;
import com.kerv.careers.utils.CommonUtils;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class JobApplication extends Base {

	private HomePage homepage;
	private JobOpportunitiesPage jobpage;
	private JobApplyPage jobApplyPage;

	@Before
	public void setUp() throws IOException {

		initializeDriver();
		this.homepage = new HomePage(driver);
		this.jobpage = new JobOpportunitiesPage(driver);
		this.jobApplyPage = new JobApplyPage(driver);
	}

	@Given("the applicant visits the Kerv website")
	public void visitLandingPage() {
		visitHomePage();
		CommonUtils.acceptCookies(driver, "Accept all");
	}

	@When("the applicant hovers over {string} link")
	public void hoveronMainMenu(String menuItemValue) {
		homepage.hoverOnMenuItem(menuItemValue);

	}

	@And("selects {string}")
	public void selectCareerOption(String careersDropDownValue) {
		homepage.clickCareerOption(careersDropDownValue);
	}

	@And("enters {string} in search box")
	public void searchVancancy(String role) {
		jobpage.searchVacancies(role);
	}

	@And("select {string} job link")
	public void selectJobRole(String role) {
		jobpage.clickJobLink(role);
		CommonUtils.switchWindow(driver);
	}

	@And("enters the applicant details in the job form")
	public void enterApplicantDetails(DataTable table) {

		List<Map<String, String>> rows = table.asMaps(String.class, String.class);

		for (Map<String, String> columns : rows) {
			jobApplyPage.enterFirstName(columns.get("FirstName"));
			jobApplyPage.enterLastName(columns.get("LastName"));
			jobApplyPage.enterEmail(columns.get("Email"));
			jobApplyPage.enterPhone(columns.get("Phone"));
			jobApplyPage.uploadCoverLetter(columns.get("CoverLetter"));
			jobApplyPage.uploadCV(columns.get("CV"));
		}
	}

	@And("clicks on recaptcha check box")
	public void clickRecaptcha() throws InterruptedException {
		jobApplyPage.selectReCaptchaBox();
		Thread.sleep(1000);
	}

	@And("submit the form")
	public void submitForm() {
		jobApplyPage.submit();
	}

	@Then("the applicant successfully applies with message {string}")
	public void verifySuccessfulApplication(String expectedMessage) {
		jobApplyPage.verifySuccessfulSubmission(expectedMessage);
	}

	@After
	public void tearDown(Scenario scenario) {
		if (scenario.isFailed()) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.attach(screenshot, "image/png", scenario.getName());
		}
		
		this.cleanUp();
		
	}
}
