Feature: New job application

  Scenario Outline: Successfully search and apply for a job opening
    Given the applicant visits the Kerv website
    When the applicant hovers over "Careers" link
    And selects "Job Opportunities"
    And enters "UX Designer" in search box
    And select "UX Designer - Junior" job link
    And enters the applicant details in the job form
      | FirstName   | LastName   | Email   | Phone   | CoverLetter       | CV       |
      | <FirstName> | <LastName> | <Email> | <Phone> | <CoverLetterPath> | <CVPath> |
    And clicks on recaptcha check box
    And submit the form
    Then the applicant successfully applies with message "Thanks for getting in touch. A member of our team will be in touch shortly."

    Examples: 
      | FirstName | LastName | Email      | Phone      | CoverLetterPath                                   | CVPath                                   |
      | Test1     | User1    | SB@abc.com | 1234567890 | src\\test\\resources\\dataFiles\\CoverLetter.docx | src\\test\\resources\\dataFiles\\CV.docx |
